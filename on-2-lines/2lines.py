# core
import re

# 3rd party
from plumbum import cli

# local




email_start_re = re.compile(r'[\w.]+@\S+')
second_line_re = re.compile(r'[\w.]+')

def email_start(s):
    m = re.search(email_start_re, s)
    if m:
        return m.group(0)
    else:
        return None

def second_line(s):
    m = re.search(second_line_re, s)
    if m:
        return m.group(0)
    else:
        return None

def slice(offset, amount):
    _offset = 0
    _amount = 0
    with open('out.dat', 'w') as o:
        with open('2lines.dat', 'r') as i:
            for line in i:
                line = line.strip()
                e = email_start(line)
                if e:
                    next_line = second_line(i.readline())
                    found = e + next_line

                    #print("FOUND {}".format(found))

                    _offset += 1

                    if _offset >= offset:
                        _amount += 1

                    print("{},{},{},{}".format(_amount, amount, offset, found))

                    if _amount > amount:
                        return


class MyApp(cli.Application):
    PROGNAME = "2 Line Email Extractor"
    VERSION = "1.0"

    offset = cli.SwitchAttr("--offset", int, default=0, help="How many emails to skip")
    amount = cli.SwitchAttr("--amount", int, default=100, help="How many emails to return")

    def main(self, offset=0, amount=100):
        slice(offset, amount)


if __name__ == '__main__':
    MyApp.run()
